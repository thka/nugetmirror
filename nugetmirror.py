#!/usr/bin/env python3

# Mirror nuget files
# Copyright 2024 Thomas Karlsson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Thomas Karlsson thomas.karlsson relea.se

import os
import argparse
import requests
import json
import sys
import re
import semver
from typing import Tuple, Dict, Any, List, Union
from urllib.parse import urlparse

proxies: Dict[str, str] = {}
verify = True


class Packagedetails():
    def __init__(self):
        self.data = {}
        self.ok = False

    def load(self, raw_json: str) -> bool:
        try:
            self.data = json.loads(raw_json)
            self.ok = True
        except json.decoder.JSONDecodeError:
            self.data = {}
            self.ok = False

        return self.ok

    def version(self) -> str:
        if 'version' in self.data:
            return self.data['version']

        return ''

    def id(self) -> str:
        if 'id' in self.data:
            return self.data['id']

        return ''

    def details_url(self) -> str:
        if '@id' in self.data:
            return self.data['@id']

        return ''

    # https://api.nuget.org/v3-flatcontainer/{id-lower}/{version-loggwer}/{id-lower}.{version-lower}.nupkg
    def package_url(self) -> str:
        # ./v3-flatcontainer/bakery.template.vb/2.0.0/bakery.template.vb.2.0.0.nupkg
        url = 'https://api.nuget.org/v3-flatcontainer/' + self.id().lower() + '/' + \
            self.version().lower() + '/' + self.id().lower() + '.' + \
            self.version().lower() + '.nupkg'

        return url

    def package_url_v2(self) -> str:
        url = 'https://www.nuget.org/api/v2/package/' + self.id() + '/' + self.version()

        return url

    def local_path(self) -> str:
        urlobject = urlparse(self.package_url())

        return urlobject.path[1:]

    def local_path_v2(self) -> str:
        urlobject = urlparse(self.package_url_v2())

        return urlobject.path[1:]

    def createdirectories(self, newpath: str) -> bool:
        onlypath = os.path.dirname(newpath)
        os.makedirs(onlypath, exist_ok=True)

        return True

    def save(self, save_path: str = None, overwrite: bool = False):
        if save_path is None:
            urlobject = urlparse(self.details_url())
            details_file = urlobject.path[1:]
        else:
            details_file = save_path

        if os.path.exists(details_file) and not overwrite:
            return True

        if self.createdirectories(details_file):
            with open(details_file, 'w') as detailsfh:
                detailsfh.write(json.dumps(self.data))

            return True

        return False


class Catalogpage():
    def __init__(self):
        self.data = {}
        self.ok = False

    def load(self, raw_json: str) -> bool:
        try:
            self.data = json.loads(raw_json)
            self.ok = True
        except json.decoder.JSONDecodeError:
            self.data = {}
            self.ok = False

        return self.ok

    def items(self, sorted_list: bool = True) -> List[Dict[str, str]]:
        if 'items' not in self.data:
            return []

        new_sorted = []
        if sorted_list:
            new_sorted = sorted(self.data['items'], key=lambda d: d['commitTimeStamp'])

            return new_sorted

        return self.data['items']

    def item(self, item_id: str) -> Dict[str, str]:
        for one in self.items():
            if one['@nuget:id'] == item_id:
                return one

        return {}


class Catalogroot():
    def __init__(self):
        self.data = {}
        self.ok = False

    def load(self, raw_json: str) -> bool:
        try:
            self.data = json.loads(raw_json)
            self.ok = True
        except json.decoder.JSONDecodeError:
            self.data = {}
            self.ok = False

        return self.ok

    def items(self, sorted_list: bool = True) -> List[Dict[str, str]]:
        if 'items' not in self.data:
            return []

        new_sorted = []
        if sorted_list:
            new_sorted = sorted(self.data['items'], key=lambda d: d['commitTimeStamp'])
            # self.data.sort(key=lambda k : k['commitTimeStamp'])

            return new_sorted

        return self.data['items']

    def item(self, item_type: str) -> Dict[str, str]:
        for one in self.items():
            if item_type == one['@type']:
                return one

        return {}


class Apiindex():
    def __init__(self):
        self.data = {}
        self.ok = False

    def load(self, raw_json: str) -> bool:
        try:
            self.data = json.loads(raw_json)
            self.ok = True
        except json.decoder.JSONDecodeError:
            self.data = {}
            self.ok = False

        return self.ok

    def version(self) -> str:
        if 'version' not in self.data:
            return ''

        return self.data['version']

    def resource_types(self) -> List[str]:
        types = []
        for one in self.resources():
            if '@type' in one:
                types.append(one['@type'])

        return types

    def resources(self) -> List[Dict[str, str]]:
        if 'resources' not in self.data:
            return []

        return self.data['resources']

    def resource_url(self, res: str) -> str:
        for one in self.resources():
            if one['@type'] == res:
                if '@id' in one:
                    return one['@id']

        return ''

    def resource_comment(self, res: str) -> str:
        for one in self.resources():
            if res in one:
                if 'comment' in one:
                    return one['comment']

        return ''

    def packagebaseaddress(self) -> str:
        pba = self.resource_url('PackageBaseAddress/3.0.0')
        # if '@id' in pba:
        return pba
        # "@type": "PackageBaseAddress/3.0.0",
        # "comment": "Base URL of where NuGet packages are stored, in the format https://api.nuget.org/v3-flatcontainer/{id-lower}/{version-lower}/{id-lower}.{version-lower}.nupkg"


class ConfigurationPacket():
    def __init__(self, packetname: str, packetdetails: Dict[str, Any]):
        self.packetname = packetname
        self.packet = packetdetails
        self.version: str = ''
        self.versioncompare: str = ''
        self.enabled = True
        if 'version' in self.packet:
            version_match = re.search(r'^([<>=]+)? *(.*)', self.packet['version'])
            if version_match[1] is None:
                self.versioncompare = ''
                self.version = self.normalise_version(version_match[0])
            else:
                self.versioncompare = version_match[1]
                self.version = self.normalise_version(version_match[2])
        else:
            self.versioncompare = ''
            self.version = ''

        if 'enabled' in self.packet:
            self.enabled = self.packet['enabled']

    def normalise_version(self, fix_version: str) -> str:
        if fix_version.count('.') > 2:
            new = re.match(r'(.+?\..+?\..+?)\.', fix_version)
            return new[1]
        if fix_version.count('.') == 2:
            return fix_version
        if fix_version.count('.') == 1:
            return fix_version + '.0'
        if fix_version.count('.') == 0:
            return fix_version + '.0.0'

        return fix_version

    def version_compare_to(self, external_version: str) -> bool:
        ret = False
        result = semver.VersionInfo.parse(self.version).compare(self.normalise_version(external_version))
        if result == 0:
            if '=' in self.versioncompare or self.versioncompare == '':
                ret = True
        if result == -1:
            if '>' in self.versioncompare:
                ret = True
        if result == 1:
            if '<' in self.versioncompare:
                ret = True

        return ret


class Configuration():
    def __init__(self, configfile: str):
        self.data = {}
        self.packagedata: Dict[str, Any] = {}
        self.options: Dict[str, Any] = {}

        if not os.path.exists(configfile):
            print('Configuration not found')

        with open(configfile, 'rt') as cfh:
            self.data = json.loads(cfh.read())

        if 'packages' in self.data.keys():
            for packet in self.data['packages'].keys():
                self.packagedata[packet] = ConfigurationPacket(packet, self.data['packages'][packet])
        if 'configuration' in self.data.keys():
            self.options = self.data['configuration']

    def packages(self, include_disabled: bool = False) -> List[str]:
        return [x for x in self.packagedata.keys() if self.packagedata[x].enabled or include_disabled]

    def package(self, pack: str) -> ConfigurationPacket:
        if pack in self.packages(include_disabled=True):
            return self.packagedata[pack]

        return ConfigurationPacket(pack, {})

    def disable_all_but(self, package_names: List[str] = []) -> None:
        for pack in self.packages(include_disabled=True):
            if self.package(pack).packetname not in package_names:
                self.package(pack).enabled = False
            else:
                self.package(pack).enabled = True

    def enable_all_but(self, package_names: List[str] = []) -> None:
        for pack in self.packages(include_disabled=True):
            if self.package(pack).packetname not in package_names:
                self.package(pack).enabled = True
            else:
                self.package(pack).enabled = False

    def http_proxy(self) -> Union[str, None]:
        if 'http_proxy' in self.options:
            return self.options['http_proxy']

        return None

    def https_proxy(self) -> Union[str, None]:
        if 'https_proxy' in self.options:
            return self.options['https_proxy']

        return None

    def indices_directory(self) -> str:
        if 'indices_directory' in self.options:
            return self.options['indices_directory']

        return '.'

    def packages_directory(self) -> str:
        if 'packages_directory' in self.options:
            return self.options['packages_directory']

        return '.'

    def packageinfo_directory(self) -> str:
        if 'packageinfo_directory' in self.options:
            return self.options['packageinfo_directory']

        return '.'

def create_directories(newpath: str) -> bool:
    onlypath = os.path.dirname(newpath)
    os.makedirs(onlypath, exist_ok=True)

    return True


def fetchfile(url: str, localpath: str, return_content: bool = False) -> Tuple[bool, Any]:
    """Fetch a file
       Input: URL and path where to save the file, known hash of file
       Output: local path to the saved file
    """
    global proxies
    global verify
    if return_content:
        stream = False
    else:
        stream = True
    if not return_content:
        if not create_directories(localpath):
            return False, "Error creating directories for %s" % localpath

    try:
        request = requests.get(url, timeout=(5, 120), stream=stream, proxies=proxies, verify=verify)
        if request.status_code != 200:
            return False, "HTTP error %d" % request.status_code

        # https://requests.readthedocs.io/en/latest/user/quickstart/#errors-and-exceptions
        if not return_content:
            with open(localpath, 'wb') as handle:
                for block in request.iter_content(1024):
                    handle.write(block)
        else:
            return True, request.content
    except requests.exceptions.ReadTimeout as connerror:
        return False, f'[WARN] Read timeout fetching {url} {connerror}'
    except requests.exceptions.ConnectionError as connerror:
        return False, f'[WARN] Connection error while fetching {url} {connerror}'
    except requests.exceptions.RequestException as connerror:
        return False, f'[WARN] Request exception when fetching {url} {connerror}'

    return True, localpath


def fetch_api_index(url: str) -> str:
    result, saved_content = fetchfile(url, localpath='', return_content=True)
    if result:
        return saved_content

    return '{}'


def fetch_catalog_root(url: str) -> str:
    result, saved_content = fetchfile(url, localpath='', return_content=True)
    if result:
        return saved_content

    return '{}'


def download_indices(config: Configuration, root_catalog: Catalogroot) -> None:
    for one in root_catalog.items():
        if not os.path.exists(os.path.join(config.indices_directory(), os.path.basename(one["@id"]))):
            if fetchfile(one["@id"], os.path.join(config.indices_directory(), os.path.basename(one["@id"]))):
                print(f'Downloaded {one["@id"]}')
                # print(f'url is {one["@id"]}')


def catalog_pages(directory: str) -> List[str]:
    if not os.path.exists(directory):
        return []

    return os.listdir(directory)


def package_data(config: Configuration, url: str) -> Dict[str, Any]:
    urlobject = urlparse(url)
    result = True
    if not os.path.exists(config.packageinfo_directory() + urlobject.path):
        result, save_path = fetchfile(url, config.packageinfo_directory() + urlobject.path)
        print(f'Downloaded {url}')
    if result:
        with open(config.packageinfo_directory() + urlobject.path, 'rt') as datafh:
            return json.loads(datafh.read())
    else:
        print(f'Error downloading package.json {url} ({save_path})')

    return {}


def create_queue(config: Configuration) -> List[Dict[str, str]]:
    queue: List[Dict[str, str]] = []
    for one_page in catalog_pages(config.indices_directory()):
        with open(os.path.join(config.indices_directory(), one_page), 'r') as jfh:
            pagefile = Catalogpage()
            if not pagefile.load(jfh.read()):
                print(f'[WARN] Couldn\'t open {os.path.join(config.indices_directory(), one_page)}')

        for item in pagefile.items():
            if item['nuget:id'] in config.packages():
                if config.package(item['nuget:id']).version_compare_to(item["nuget:version"]):
                    queue.append(item)
                else:
                    print(f'{item["nuget:version"]} is not the correct version')

    return queue


# https://www.nuget.org/api/v2/package/Rebus.MongoDb/0.98.2 -> file rebus.mongodb.0.98.2.nupkg
def main():
    parser = argparse.ArgumentParser(description='Nuget mirror')
    parser.add_argument('-c',
                        '--config',
                        dest='configfile',
                        action='store',
                        default='nugetmirror.json',
                        help='Configuration file',
                        required=False)
    parser.add_argument('-v',
                        '--verbose',
                        dest='verbose',
                        default=False,
                        action='store_true',
                        help="Verbose output",
                        required=False)
    parser.add_argument('-i',
                        '--indices',
                        dest='indices',
                        default=False,
                        action='store_true',
                        help="Download all page indices",
                        required=False)
    parser.add_argument('-u',
                        '--update',
                        dest='update',
                        default=False,
                        action='store',
                        nargs='*',
                        metavar='repos',
                        help='Update repos',
                        required=False)

    args = parser.parse_args()

    configuration = Configuration(args.configfile)
    if configuration.http_proxy() is not None:
        proxies['http'] = configuration.http_proxy()
    if configuration.https_proxy() is not None:
        proxies['https'] = configuration.https_proxy()

    if args.update and len(args.update) > 0:
        configuration.disable_all_but(args.update)

    queue_list: List[Dict[str, str]] = []
    api_index_url = 'https://api.nuget.org/v3/index.json'
    print(f'[INFO] Fetching api index from {api_index_url}')
    api = Apiindex()
    if not api.load(fetch_api_index(api_index_url)):
        print(f'[ERROR] Couldn\'t load {api_index_url}')
        sys.exit(1)
    # print(api.resource_url('Catalog/3.0.0'))
    print(f"[INFO] Fetching root catalog from {api.resource_url('Catalog/3.0.0')}")
    rootcatalog = Catalogroot()
    if not rootcatalog.load(fetch_catalog_root(api.resource_url('Catalog/3.0.0'))):
        print(f'[ERROR] Couldn\'t load {api.resource_url("Catalog/3.0.0")}')
        sys.exit(1)

    if args.indices:
        if args.verbose:
            print('[INFO] Downloading page indices')
        download_indices(configuration, rootcatalog)

    queue_list = create_queue(configuration)
    # Download binaries
    for one_package in queue_list:
        result, message = fetchfile(one_package['@id'], '', return_content=True)
        if not result:
            print(f'[WARN] Failed to download {one_package["@id"]}')
            continue

        package_info = Packagedetails()
        if not package_info.load(message.decode()):
            print(f'[WARN] Couldn\'t load {one_package["@id"]}, continuing with the next one')
            continue
        if not package_info.save():
            print(f'[WARN] Failed to save package details for {package_info.id()}.{package_info.version()}')

        if not os.path.exists(os.path.join(configuration.packages_directory(), package_info.local_path())):
            print(f'[INFO] Downloading {package_info.package_url()}')
            result, message = fetchfile(package_info.package_url(), os.path.join(configuration.packages_directory(), package_info.local_path()))
        if not result:
            print('[WARN] Failed to download package for api version 3.0.0, trying v2')
            if not os.path.exists(os.path.join(configuration.packages_directory(), package_info.local_path_v2())):
                if fetchfile(package_info.package_url_v2(), os.path.join(configuration.packages_directory(), package_info.local_path_v2())):
                    print('[INFO] Downloaded package throught api v2')
                else:
                    print(f'[WARN] Failed to download {package_info.package_url_v2()}')


if __name__ == '__main__':
    main()
