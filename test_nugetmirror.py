#!/usr/bin/env python3

import os
from nugetmirror import Packagedetails, Configuration


def test_packagedetails():
    with open('test_api.nuget.org_v3_catalog0_data_2018.12.15.03.33.48_rebus.mongodb.0.98.2.json', 'rt') as packfh:
        jsondata = packfh.read()

    package = Packagedetails()
    assert package.load(jsondata) is True
    assert package.details_url() == 'https://api.nuget.org/v3/catalog0/data/2018.12.15.03.33.48/rebus.mongodb.0.98.2.json'
    assert package.version() == '0.98.2'
    assert package.id() == 'Rebus.MongoDb'
    assert package.package_url() == 'https://api.nuget.org/v3-flatcontainer/rebus.mongodb/0.98.2/rebus.mongodb.0.98.2.nupkg'
    assert package.package_url_v2() == 'https://www.nuget.org/api/v2/package/Rebus.MongoDb/0.98.2'
    assert package.local_path() == 'v3-flatcontainer/rebus.mongodb/0.98.2/rebus.mongodb.0.98.2.nupkg'
    assert package.local_path_v2() == 'api/v2/package/Rebus.MongoDb/0.98.2'
    assert package.save('./_test_rebus.mongodb.json') is True
    assert os.path.exists('./_test_rebus.mongodb.json') is True
    os.unlink('./_test_rebus.mongodb.json')


def test_configuration():
    configuration = Configuration('test_nugetmirror.json')
    assert configuration.https_proxy() == 'http://localhost:8080'
    assert configuration.http_proxy() is None
    assert configuration.indices_directory() == 'cache'
    assert configuration.packages_directory() == 'packages'
    assert configuration.packageinfo_directory() == 'packageinfo'

    assert configuration.packages() == ['Bakery.Template.VB', 'structuremap', 'testpackage',
                                        'test_greater_equal', 'test_no_version']
    assert configuration.packages(include_disabled=True) == ['Bakery.Template.VB', 'structuremap', 'testpackage',
                                                             'test_greater_equal', 'test_no_version',
                                                             'test_not_enabled']
    structuremap = configuration.package('structuremap')
    assert structuremap.version == '2.6.2'
    assert structuremap.versioncompare == '>'
    no_version = configuration.package('test_no_version')
    assert no_version.version == ''
    assert no_version.normalise_version('1') == '1.0.0'
    assert no_version.normalise_version('2.0') == '2.0.0'
    assert no_version.normalise_version('3.0.0') == '3.0.0'
    assert no_version.normalise_version('4.0.0.0') == '4.0.0'
    assert no_version.normalise_version('5.0.0.0.0') == '5.0.0'
    test_greater_equal = configuration.package('test_greater_equal')
    assert test_greater_equal.version == '2.6.2'
    assert test_greater_equal.versioncompare == '>='
    assert test_greater_equal.version_compare_to('1.0') is False
    assert test_greater_equal.version_compare_to('2.6') is False
    assert test_greater_equal.version_compare_to('2.6.2') is True
    assert test_greater_equal.version_compare_to('2.6.3') is True
    assert test_greater_equal.version_compare_to('3.6.3') is True

    configuration.disable_all_but(['structuremap'])
    assert configuration.packages() == ['structuremap']
    configuration.enable_all_but(['structuremap'])
    assert configuration.packages() == ['Bakery.Template.VB', 'testpackage',
                                        'test_greater_equal', 'test_no_version',
                                        'test_not_enabled']
